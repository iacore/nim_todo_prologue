import prologue
import nimja
from simpleflake import nil

import std/[tables,strutils]

template renderTemplate(name: string): untyped =
  compileTemplateFile(getScriptDir() / "../templates" / name)

var todos: Table[uint64, string]

proc index(): string =
  renderTemplate("index.html")

proc hello*(ctx: Context) {.async.} =
  {.cast(gcsafe).}:
    resp index()

proc addItem(ctx: Context) {.async.} =
  let todoContent = ctx.request.postParams["content"]
  {.cast(gcsafe).}:
    todos[simpleflake.gen()] = todoContent
  # echo ctx.request.formParams.data['content'].body
  resp redirect("/", Http302)

proc deleteItem(ctx: Context) {.async.} =
  let id = ctx.request.postParams["id"].parseInt.uint64
  {.cast(gcsafe).}:
    todos.del(id)
  resp redirect("/", Http303)

let app = newApp()
app.get("/", hello)
app.post("/add", addItem)
app.post("/delete", deleteItem)
app.run()
