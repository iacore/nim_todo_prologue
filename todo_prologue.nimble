# Package

version       = "0.1.0"
author        = "Locria Cyber"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"
bin           = @["todo_prologue"]


# Dependencies

requires "nim >= 1.6.10"
requires "prologue >= 0.6.4"
requires "nimja >= 0.8.6"
requires "simpleflake >= 0.1.1"
